module gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/plugins/local-provider

go 1.21.4

toolchain go1.21.7

require (
	gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/core v0.0.0-20240205175452-992282f633c4
	gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/engine/test v0.0.0-20240206152112-22a761cda723
)

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.2.0 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/lestrrat-go/blackmagic v1.0.2 // indirect
	github.com/lestrrat-go/httpcc v1.0.1 // indirect
	github.com/lestrrat-go/httprc v1.0.4 // indirect
	github.com/lestrrat-go/iter v1.0.2 // indirect
	github.com/lestrrat-go/jwx/v2 v2.0.19 // indirect
	github.com/lestrrat-go/option v1.0.1 // indirect
	github.com/segmentio/asm v1.2.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
)
